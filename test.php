<?php
//start pagination

    //set to first page if no page selected
    $page = isset($_GET['page']) ? $_GET['page'] : 1;

    //set number of rows per page
    $rowPage = 20;

    //calculate for limit clause
    $startLimit = ($rowPage * $page) - $rowPage;


//end setting pagination

include_once 'config/db.php';
include_once 'obj/customer.php';
$page_title = "List Customer";
//create db and objects
$database = new DB();
$db = $database->getConnection();
$customer = new Customer($db);

    //counts total num of records

//call read data customers
$stmt = $customer->summonLimit($startLimit,$rowPage);

$rCount = $stmt->rowCount();
#$rowResult = $stmt->fetchAll(PDO::FETCH_ASSOC);
include_once 'lib/pg_header.php';

 

    ## This is where the pagination.php gonna be ##
    # echo "test<br>";
    include_once "lib/pagination.php";

    ## End of pagination ##
    if ($rCount>0){
      echo "<br><table class='table table-hover' border='0' style='margin-left: auto; margin-right: auto;'>
          
          <tr></tr>
          <tr>
            <th style='padding: 5px; text-align: center' p>Account No</th>
            <th>Company Name</th>
            <th>Address</th>
            <th>P.I.C</th>
            
            <th style='text-align: center;border-right: none; border-left: none' width ='18%' align='center'>Action</th>
            
          </tr>";
      #extracting row from db
      #foreach ($rowResult as $record) {
      # echo "<tr>";
      # foreach ($record as $key => $value) {
      #   echo "<td>".$value."</td>";
      # }
      # echo "</tr>";
      #}
      while ($rowResult = $stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($rowResult);
        echo "<tr>
            <td class='rowpad'>{$accno}</td>
            <td class='rowpad'>{$co_name}</td>
            <td class='rowpad'>{$address1}</td>
            <td class='rowpad'>{$attn_sales}</td>
            <td class='rowpad' style='text-align:center'><a href='cus_dtl.php?cid={$cid}&edt=no' class='btn btn-primary left-margin'>Details</a> <a href='cus_dtl.php?cid={$cid}&edt=yes' class='btn btn-info left-margin'> Edit </a> <a href='cus_del.php?cid={$cid}' OnClick=\"return confirm('Are you sure? Data will be lost!')\" class='btn btn-danger delete-object'> Delete </a></td>
            
            </tr>
            <tr padding='9px'></tr>";


      }
      echo "</table>";
    }
    
    ?>
  </div>
  <?php
      ## This is where the pagination.php gonna be ##
    #include_once "lib/pagination.php";
    ## End of pagination ##
  ?>
</div>    
  </body>
    <?php include_once "lib/pg_footer.php"; ?> <br>
</html>