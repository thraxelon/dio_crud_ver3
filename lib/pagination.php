<?php
##################################################
# Variable lists:
#  currently selected page 		: $page
#  number of rows per page 		: $rowPage
#  Starting number of records 	: $startLimit
##################################################
//page url
$pageurl = "test.php?";
//get number of records
$rowCounter = $customer->countAll();
#echo $rowCounter."<br>";

//get number of pages based on number of rows per page
$totPages = ceil($rowCounter/$rowPage);
#echo $totPages;

//set number of links to be shown
$pageLimit = 8;

//set link page settings
$initPagesNum = $page - $pageLimit;
$lastPagesNum = ($page + $pageLimit) + 1;


//start making pagination
echo "<table><tr>";
//check if current page is not the first page


if($page>1){
	//if not first page, then create a pagination to the First Page
	echo "<td style='padding-right: 6px'>";
		echo "<a href='{$pageurl}page=1' class='btnpage'>First</a>";
	echo "</td>";
}

for ($x=$initPagesNum; $x<$lastPagesNum; $x++){
	//check if current page
	if (($x>0) && ($x<=$totPages)) {
		if ($x == $page) {
			echo "<td style='padding-right: 6px'";
			echo "<a class='btnlist'><b style='font-weight:bold'>$x</b></a>";
			echo "</td>";
		}else{
			echo "<td style='padding-right: 6px'>";
			echo "<a href='{$pageurl}page=$x' class='btnlist'>$x</a>";
			echo "</td>";
		}
	}
		

}
//create Last Link
if ($page<$totPages) {
	echo "<td><a href={$pageurl}page={$totPages} class='btnpage'>Last</a>";
}
echo "</tr></table>"
?>