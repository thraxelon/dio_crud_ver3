<?php
include_once "obj/customer.php";
include_once "obj/admin.php";


$id = isset($_GET['cid']) ? $_GET['cid'] : die('ERROR: You can\'t reach this page this way. <br> 
												 Please do it the  <a href="cus_list.php">right way</a>.');
$edt = isset($_GET['edt']) ? $_GET['edt'] : "no";
$rdo= "";
if ($edt == "no") {
	$rdo = 'readonly = "readonly"';
	$dsb ='disabled';
	$page_title = "Detail Customer";
}else{
	$rdo = '';
	$dsb = '';
	$page_title = "Edit Customer";
}

$customer = new Customer();
$admin = new Admin();

$cusResult = $customer->summonOne($id);
$admResult = $admin->summonSalesman();

//Get the name of Salesperson by 

include_once 'lib/pg_header.php';
?>


	<body>

	<!--	<form action="cus_upd.php" method="POST">
			<table>
				<tr>
					<td>Account No</td>
					<td>:</td>
					<td><input type="text" name="accno" value="<?php echo $acc_no; ?>" readonly></input></td>
				</tr>
				<tr>
					<td>Company Name</td>
					<td>:</td>
					<td><input type="text" <?php echo $rdo; ?> name="cname" value="<?php echo $cname; ?>"></input></td>
				</tr>
				<tr>
					<td>Address</td>
					<td>:</td>
					<td><input type="text" <?php echo $rdo; ?> name="adr1" value="<?php echo $address1; ?>"></input></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><input type="text" <?php echo $rdo; ?> name="adr2" value="<?php echo $address2; ?>"></input></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><input type="text" <?php echo $rdo; ?> name="adr3" value="<?php echo $address3; ?>"></input></td>
				</tr>
				<tr>
					<td>Person in Charge</td>
					<td>:</td>
					<td><input type="text" <?php echo $rdo; ?> name="attn" value="<?php echo $attn; ?>"></input></td>
				</tr>
				<tr>
					<td>Contact</td>
					<td>:</td>
					<td><input type="text" <?php echo $rdo; ?> name="contact" value="<?php echo $contact; ?>"></input></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="right"><?php
							#if ($edt=="no") {
								
							#}else{
							#	echo '<input  type="submit" style="visibility: visible;" value="Submit">';
							#}
					?>
					</td>
				</tr>
			</table> -->
		<form action="cus_upd.php?cid=<?php echo $id;?>" method="POST">
			<table class="rowcontent" style="">
					<tr>
						<td>Account No.</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['accno']; ?>" name="accno"> </td>
					</tr>
					<tr>
						<td>Company Name</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['co_name']; ?>" name="co_name"></td>
					</tr>
					<tr>
						<td>Company No.</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['co_no']; ?>" name="co_no"></td>
					</tr>
					<tr>
						<td>Company Code</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['co_code']; ?>" name="co_code"></td>
					</tr>
					<tr>
						<td>Address 1</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['address1']; ?>" name="adr1"></td>
					</tr>
					<tr>
						<td>Address 2</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['address2']; ?>" name="adr2"></td>
					</tr>
					<tr>
						<td>Address 3</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['address3']; ?>" name="adr3"></td>
					</tr>
					<tr>
						<td>Country</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['country']; ?>" name="country"></td>
					</tr>
					<tr>
						<td>Sales Telephone</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['telephone_sales']; ?>" name="salesphone"></td>
						<td style="width: 10%"></td>
						<td>Acc Telephone</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['telephone_acc']; ?>" name="accphone"></td>
					</tr>
					<tr>
						<td>Sales Fax</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['fax_sales']; ?>" name="salesfax"></td>
						<td style="width: 10%"></td>
						<td>Acc Fax</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['fax_acc']; ?>" name="accfax"></td>
					</tr>
					<tr>
						<td>Sales Handphone</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['handphone_sales']; ?>" name="saleshp"></td>
						<td style="width: 10%"></td>
						<td>Acc Handphone</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['handphone_acc']; ?>" name="acchp"></td>
					</tr>
					<tr>
						<td>Sales Email</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['email_sales']; ?>" name="salesmail"></td>
						<td style="width: 10%"></td>
						<td>Acc Email</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['email_acc']; ?>" name="accmail"></td>
					</tr>
					<tr>
						<td>Sales Attn.</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['attn_sales']; ?>" name="salesattn"></td>
						<td style="width: 10%"></td>
						<td>Acc Attn.</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['attn_acc']; ?>" name="accattn"></td>
					</tr>
					<tr>
						<td><br></td>
					</tr>
					<tr>
						<td><br></td>
					</tr>
					<tr>
						<td>Group</td>
						<td>:  <select <?php echo $dsb; ?> name="group" class="width-sz" >
							   <!--<select name="group" class="width-sz" style="width: 187px">-->
							   	<?php
							   	$o1 = ($cusResult['groups'] == "Group A") ? 'selected' : '';
							   	$o2 = ($cusResult['groups'] == "Group B") ? 'selected' : '';
							   	$o3 = ($cusResult['groups'] == "Group C") ? 'selected' : '';
							   	$o4 = ($cusResult['groups'] == "Group F") ? 'selected' : '';
							   	$o5 = ($cusResult['groups'] == "Group O") ? 'selected' : '';

								echo '<option value="Group A"'.$o1.'>Group A</option>
								<option value="Group B"'.$o2.'>Group B</option>
								<option value="Group C"'.$o3.'>Group C</option>
								<option value="Group F"'.$o4.'>Group F</option>
								<option value="Group O"'.$o5.'>Group O</option>'
								?>
							   </select>
							   <!--<input type="text" name="group"></td>-->
						<td style="width: 10%"></td>
						<td>Salesperson</td>
						<td>:  <select <?php echo $dsb; ?> name="salesperson" class="width-sz">
							   <!--<select name="salesperson" style="width: 187px">--> 	
								<?php
								#fills combo box with Salesperson Name
									foreach ($admResult as $value) {
										$s1 = ($cusResult['aid_cus'] == $value['aid']) ? 'selected' : '';
										echo "<option value='{$value['aid']}'".$s1.">{$value['username']}</option>";
									}
								?>
							   <!--</select	><input type="text" name="salesperson"></td>-->
					</tr>
					<tr>
					</tr>
					<tr>
						<td>Currency</td>
						<td>:  <select <?php echo $dsb; ?> name="currency" class="width-sz" >
							   <!--<select name="group" class="width-sz" style="width: 187px">-->
							   	<?php
							   	$c1 = ($cusResult['currency'] == 1) ? 'selected' : '';
							   	$c2 = ($cusResult['currency'] == 2) ? 'selected' : '';

							   	echo '<option value="1"'.$c1.'>RM - Malaysia Ringgit</option>
								<option value="2"'.$c2.'>RP - Indonesia Rupiah</option>';
							   	?>
								
							   </select>
						<td style="width: 10%"></td>
						<td>Terms</td>
						<td>:  <select <?php echo $dsb; ?> name="terms" class="width-sz" >
								<?php
							   	$t1 = ($cusResult['terms'] == "COD") ? 'selected' : '';
							   	$t2 = ($cusResult['terms'] == "15 Days") ? 'selected' : '';
							   	$t3 = ($cusResult['terms'] == "30 Days") ? 'selected' : '';
							   	$t4 = ($cusResult['terms'] == "45 Days") ? 'selected' : '';
							   	$t5 = ($cusResult['terms'] == "60 Days") ? 'selected' : '';
							   	$t6 = ($cusResult['terms'] == "90 Days") ? 'selected' : '';
							   	$t7 = ($cusResult['terms'] == "120 Days") ? 'selected' : '';
							   	$t8 = ($cusResult['terms'] == "PBO") ? 'selected' : '';

								echo '<option value="COD"'.$t1.'>COD</option>
								<option value="15 Days"'.$t2.'>15 Days</option>
								<option value="30 Days"'.$t3.'>30 Days</option>
								<option value="45 Days"'.$t4.'>45 Days</option>
								<option value="60 Days"'.$t5.'>60 Days</option>
								<option value="90 Days"'.$t6.'>90 Days</option>
								<option value="120 Days"'.$t7.'>120 Days</option>
								<option value="PBO"'.$t8.'>P.B.O - Payment Before Order</option>'
								?>
							   <!--<select name="group" class="width-sz" style="width: 187px">-->
								
							   </select>
						
					</tr>
					<tr>
						
					</tr>
					<tr>
						<td>Company</td>
						<td>:  <select <?php echo $dsb; ?> name="company" class="width-sz" >
							   <!--<select name="group" class="width-sz" style="width: 187px">-->
								<option value="PTPHH" selected>PT. PHH</option>
							   </select>
						<td style="width: 10%"></td>
						<td>Credit Limit</td>
						<td>:  <input type="text" <?php echo $rdo; ?> value="<?php echo $cusResult['credit_limit']; ?>" name="credit_limit"></td>
						
					</tr>
					<tr>
						
					</tr>
					<tr>
						<td>Date Created</td>
						<td>:  <input type="text" readonly value="<?php echo $cusResult['date_created']; ?>" name="tdate"></td>
						<td style="width: 10%"></td>
						<td>Status</td>
						<td>:  <select <?php echo $dsb; ?> name="status" class="width-sz" >
							   <!--<select name="group" class="width-sz" style="width: 187px">-->
							   	<?php
							   	$st1 = ($cusResult['status'] == "active") ? 'selected' : '';
							   	$st2 = ($cusResult['status'] == "hold") ? 'selected' : '';
							   	$st3 = ($cusResult['status'] == "disabled") ? 'selected' : '';
							   	echo '<option value="active"'.$st1.'>Active</option>
								<option value="hold"'.$st2.'>Hold</option>
								<option value="disabled"'.$st3.'>Disabled</option>';
							   	?>

								
							   </select>
					</tr>
					<tr>
						<td style="text-align: left;vertical-align: top">Remarks</td>
						<td style="text-align: left;vertical-align: top;height: 80px">:  <textarea <?php echo $rdo; ?> name="remarks" style="; overflow: auto; resize: none" value=""><?php echo htmlspecialchars($cusResult['remarks']);?></textarea>
					</tr>
					
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td align="right"><?php
							if ($edt=="no") {
								
							}else{
								echo '<button type="submit" name="submit" class="btn btn-primary">Update</button>';
							}
					?></td>
					<!--<td align="right">  <button type="submit" name="submit" class="btn btn-primary">Submit</button></td>-->
					
					</tr>
				</table>
		</form>
	</body>
		<?php include_once "lib/pg_footer.php"; ?>
	</html>
