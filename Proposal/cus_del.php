<?php
include_once 'obj/customer.php';

$page_title = "Delete Customer";

$id = isset($_GET['cid']) ? $_GET['cid'] : die('ERROR: You can\'t reach this page this way. <br> 
												 Please do it the  <a href="cus_list.php">right way</a>.');

include_once 'lib/pg_header.php';


//create objects
$customer = new Customer();
$delCustomer = $customer->byebye($id);

if ($delCustomer){
		echo "<div class='alert alert-success'>Delete Successful.<br>";
	}else{
		echo "<div class='alert alert-danger'>Failed to Delete Data!<br>";
	}

echo "Click <a href='cus_list.php'>here</a> to return.</div>	";
include_once "lib/pg_footer.php";
?>