<?php
//start including database and customer classes
include_once "obj/customer.php";
include_once "obj/admin.php";
$page_title = "Add New Customer";


//transfer connection to subclasses
$customer = new Customer();
$admin = new Admin();

//get list of salesperson from Admin Table
$rRowAdmin = $admin->summonSalesman();

include_once 'lib/pg_header.php';
?>
<?php
//If submitted, get [POST] data
if($_POST){

	#get Admin Data
	//$stmt2 = $admin->summonByName($_POST['salesperson']);
	//echo $_POST['salesperson']."<br>";
	//echo $stmt2->rowCount()."<br>";
	//$rowAdmin = $stmt2->fetch(PDO::FETCH_ASSOC);

	//extract($rowAdmin);
	//$aidcus = $aid."<br>";

	//echo $aidcus;
	#insert values to customer object

	$customer->accno = $_POST['accno']; 		#customer id
	$customer->co_name = $_POST['co_name']; 	#customer/company name
	$customer->co_no = $_POST['co_no'];		#customer Tax number
	$customer->co_code = $_POST['co_code'];	#customer Initials
	$customer->adr1 = $_POST['adr1']; 		#address1st  line
	$customer->adr2 = $_POST['adr2'];		#address 2nd Line
	$customer->adr3 = $_POST['adr3'];		#address 3rd line
	$customer->country = $_POST['country'];	#company country
	$customer->salesphone = $_POST['salesphone'];	#sales phone number
	$customer->salesfax = $_POST['salesfax'];	#sales fax number
	$customer->saleshp = $_POST['saleshp'];	#sales handphone number
	$customer->salesmail = $_POST['salesmail'];	#sales email
	$customer->salesatn = $_POST['salesattn'];	#sales name
	$customer->accphone = $_POST['accphone'];	#accounting phone number
	$customer->accfax = $_POST['accfax'];		#accounting fax number
	$customer->acchp = $_POST['acchp'];		#accounting handphone number
	$customer->accmail = $_POST['accmail'];	#accounting email
	$customer->accatn = $_POST['accattn'];		#accounting name
	$customer->groups = $_POST['group'];		#grouping code
	$customer->aid_cus = $_POST['salesperson'];	#marketing code
	$customer->terms = $_POST['terms'];		#payment terms
	$customer->currency = $_POST['currency'];	#payment currency
	$customer->creditlimit = $_POST['credit_limit'];#payment credit limits
	$customer->company = $_POST['company'];	#company branch
	$customer->status = $_POST['status'];		#customer status (active, hold, disabled)
	$customer->remarks = $_POST['remarks'];	#remarksx


	//create new customer data
	if ($customer->create()){
		echo "<div class='alert alert-success'>Insert Successful</div>";
	}else{
		echo "<div class='alert alert-danger'>Insert Failed</div>";
	}
}

?>


	<body>
		<div>
			<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
				<table class="rowcontent" style=": 1px">
					<tr>
						<td>Account No.</td>
						<td>:  <input type="text" name="accno"> </td>
					</tr>
					<tr>
						<td>Company Name</td>
						<td>:  <input type="text" name="co_name"></td>
					</tr>
					<tr>
						<td>Company No.</td>
						<td>:  <input type="text" name="co_no"></td>
					</tr>
					<tr>
						<td>Company Code</td>
						<td>:  <input type="text" name="co_code"></td>
					</tr>
					<tr>
						<td>Address 1</td>
						<td>:  <input type="text" name="adr1"></td>
					</tr>
					<tr>
						<td>Address 2</td>
						<td>:  <input type="text" name="adr2"></td>
					</tr>
					<tr>
						<td>Address 3</td>
						<td>:  <input type="text" name="adr3"></td>
					</tr>
					<tr>
						<td>Country</td>
						<td>:  <input type="text" name="country"></td>
					</tr>
					<tr>
						<td>Sales Telephone</td>
						<td>:  <input type="text" name="salesphone"></td>
						<td style="width: 10%"></td>
						<td>Acc Telephone</td>
						<td>:  <input type="text" name="accphone"></td>
					</tr>
					<tr>
						<td>Sales Fax</td>
						<td>:  <input type="text" name="salesfax"></td>
						<td style="width: 10%"></td>
						<td>Acc Fax</td>
						<td>:  <input type="text" name="accfax"></td>
					</tr>
					<tr>
						<td>Sales Handphone</td>
						<td>:  <input type="text" name="saleshp"></td>
						<td style="width: 10%"></td>
						<td>Acc Handphone</td>
						<td>:  <input type="text" name="acchp"></td>
					</tr>
					<tr>
						<td>Sales Email</td>
						<td>:  <input type="text" name="salesmail"></td>
						<td style="width: 10%"></td>
						<td>Acc Email</td>
						<td>:  <input type="text" name="accmail"></td>
					</tr>
					<tr>
						<td>Sales Attn.</td>
						<td>:  <input type="text" name="salesattn"></td>
						<td style="width: 10%"></td>
						<td>Acc Attn.</td>
						<td>:  <input type="text" name="accattn"></td>
					</tr>
					<tr>
						<td><br></td>
					</tr>
					<tr>
						<td><br></td>
					</tr>
					<tr>
						<td>Group</td>
						<td>:  <select name="group" class="width-sz" >
							   <!--<select name="group" class="width-sz" style="width: 187px">-->
								<option value="Group A">Group A</option>
								<option value="Group B">Group B</option>
								<option value="Group C">Group C</option>
								<option value="Group F">Group F</option>
								<option value="Group O" selected>Group O</option>
							   </select>
							   <!--<input type="text" name="group"></td>-->
						<td style="width: 10%"></td>
						<td>Salesperson</td>
						<td>:  <select name="salesperson" class="width-sz">
							   <!--<select name="salesperson" style="width: 187px">--> 	
								<?php
								#fills combo box with Salesperson Name
								foreach ($rRowAdmin as $value) {
									echo "<option value='{$value['aid']}'>{$value['username']}</option>";
								}
									#while ($rowresult = $stmt->fetch(PDO::FETCH_ASSOC)) {
									#	extract($rowresult);
									#	echo "<option value='{$value['aid']}'>{$value['username']}</option>";
									#}
								?>
							   <!--</select	><input type="text" name="salesperson"></td>-->
					</tr>
					<tr>
					</tr>
					<tr>
						<td>Currency</td>
						<td>:  <select name="currency" class="width-sz" >
							   <!--<select name="group" class="width-sz" style="width: 187px">-->
								<option value="1">RM - Malaysia Ringgit</option>
								<option value="2">RP - Indonesia Rupiah</option>
							   </select>
						<td style="width: 10%"></td>
						<td>Terms</td>
						<td>:  <select name="terms" class="width-sz" >
							   <!--<select name="group" class="width-sz" style="width: 187px">-->
								<option value="COD">COD</option>
								<option value="15 Days">15 Days</option>
								<option value="30 Days">30 Days</option>
								<option value="45 Days">45 Days</option>
								<option value="60 Days">60 Days</option>
								<option value="90 Days">90 Days</option>
								<option value="120 Days">120 Days</option>
								<option value="PBO">P.B.O - Payment Before Order</option>
							   </select>
						
					</tr>
					<tr>
						
					</tr>
					<tr>
						<td>Company</td>
						<td>:  <select name="company" class="width-sz" >
							   <!--<select name="group" class="width-sz" style="width: 187px">-->
								<option value="PTPHH">PT. PHH</option>
							   </select>
						<td style="width: 10%"></td>
						<td>Credit Limit</td>
						<td>:  <input type="text" name="credit_limit"></td>
						
					</tr>
					<tr>
						
					</tr>
					<tr>
						<td>Date Created</td>
						<td>:  <input type="text" name="tdate" value='<?php echo date('Y-m-d H:i:s'); ?>' readonly></td>
						<td style="width: 10%"></td>
						<td>Status</td>
						<td>:  <select name="status" class="width-sz" >
							   <!--<select name="group" class="width-sz" style="width: 187px">-->
								<option value="active">Active</option>
								<option value="hold">Hold</option>
								<option value="disabled">Disabled</option>
							   </select>
					</tr>
					<tr>
						<td style="text-align: left;vertical-align: top">Remarks</td>
						<td style="text-align: left;vertical-align: top;height: 80px">:  <textarea name="remarks" style="; overflow: auto; resize: none"></textarea>
					</tr>
					
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td align="right">  <button type="submit" name="submit" class="btn btn-primary">Create</button></td>
					</tr>
				</table>
			</form>
		</div>
	</body>
	<?php include_once "lib/pg_footer.php"; ?>
</html>