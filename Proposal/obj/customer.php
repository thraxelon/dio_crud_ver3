<?php

include_once "./config/db.inc.php";
include_once "./config/db_action.php";

class Customer{

	private $tbl_name = "customer_ptphh";

	//list of columns in table
	public $accno; 		#customer id
	public $co_name; 	#customer/company name
	public $co_no;		#customer Tax number
	public $co_code;	#customer Initials
	public $adr1; 		#address1st  line
	public $adr2;		#address 2nd Line
	public $adr3;		#address 3rd line
	public $country;	#company country
	public $salesphone;	#sales phone number
	public $salesfax;	#sales fax number
	public $saleshp;	#sales handphone number
	public $salesmail;	#sales email
	public $salesatn;	#sales name
	public $accphone;	#accounting phone number
	public $accfax;		#accounting fax number
	public $acchp;		#accounting handphone number
	public $accmail;	#accounting email
	public $accatn;		#accounting name
	public $groups;		#grouping code
	public $aid_cus;	#marketing code
	public $terms;		#payment terms
	public $currency;	#payment currency
	public $creditlimit;#payment credit limits
	public $company;	#company branch
	public $status;		#customer status (active, hold, disabled)
	public $dtime;		#when is created
	public $remarks;	#remarks
	public $credit_used;#credit that has been used
	

	//transfers connection cred to class
	public function __construct(){
		#here's to put something that's be done first
	}


	function create(){
		#set time value
		$this->dtime = date('Y-m-d H:i:s');

		#settle query

		$qr = "INSERT INTO ".$this->tbl_name."
			   SET 
			   accno='".$this->accno."',
			    co_name='".$this->co_name."', 
			    co_no='".$this->co_no."', 
			    co_code='".$this->co_code."', 
			    address1='".$this->adr1."', 
			    address2='".$this->adr2."', 
			    address3='".$this->adr3."', 
			    country='".$this->country."', 
			    telephone_sales='".$this->salesphone."', 
			    fax_sales='".$this->salesfax."', 
			    handphone_sales='".$this->saleshp."', 
			    email_sales='".$this->salesmail."', 
			    attn_sales='".$this->salesatn."', 
			    telephone_acc='".$this->accphone."', 
			    fax_acc='".$this->accfax."', 
			    handphone_acc='".$this->acchp."', 
			    email_acc='".$this->accmail."', 
			    attn_acc='".$this->accatn."', 
			    groups='".$this->groups."', 
			    aid_cus=".$this->aid_cus.", 
			    terms='".$this->terms."', 
			    currency=".$this->currency.", 
			    credit_limit=".$this->creditlimit.", 
			    company='".$this->company."', 
			    status='".$this->status."',
			    date_created='".$this->dtime."',
			    remarks='".$this->remarks."'"; 
		#echo $qr;
		$dbInsert = new SQL($qr);
		return $dbInsert->getUpdate();
	}

	//function to update product
	function update($cid){
		#settle query
		$qr = "UPDATE ".$this->tbl_name."
			   SET
			   accno='".$this->accno."',
			    co_name='".$this->co_name."', 
			    co_no='".$this->co_no."', 
			    co_code='".$this->co_code."', 
			    address1='".$this->adr1."', 
			    address2='".$this->adr2."', 
			    address3='".$this->adr3."', 
			    country='".$this->country."', 
			    telephone_sales='".$this->salesphone."', 
			    fax_sales='".$this->salesfax."', 
			    handphone_sales='".$this->saleshp."', 
			    email_sales='".$this->salesmail."', 
			    attn_sales='".$this->salesatn."', 
			    telephone_acc='".$this->accphone."', 
			    fax_acc='".$this->accfax."', 
			    handphone_acc='".$this->acchp."', 
			    email_acc='".$this->accmail."', 
			    attn_acc='".$this->accatn."', 
			    groups='".$this->groups."', 
			    aid_cus=".$this->aid_cus.", 
			    terms='".$this->terms."', 
			    currency=".$this->currency.", 
			    credit_limit=".$this->creditlimit.", 
			    company='".$this->company."', 
			    status='".$this->status."',
			    remarks='".$this->remarks."'
			    WHERE cid=".$cid; 


		#execute query
		$dbUpdate = new SQL($qr);
		return $dbUpdate->getUpdate();
	}

	//function to read product
	function summonAll(){
		#settle query
		$qr = "SELECT * FROM ".$this->tbl_name." ORDER BY accno ASC";
		 
		#execute query
		$dbSummon = new SQL($qr);
		$result = $dbSummon->fetchAllRow();
		return $result;
	}
	function summonLimit($start, $count){
		#settle query
		$qr = "SELECT * FROM ".$this->tbl_name." ORDER BY accno ASC LIMIT ".$start.",".$count;
		
		#execute query;
		$dbSummonLimit = new SQL($qr);
		$result = $dbSummonLimit->fetchOneRow();
		return $result;

	}

	//function to read one data
	function summonOne($cid){
		#settle query
		$qr = "SELECT * FROM ".$this->tbl_name." WHERE cid=".$cid." LIMIT 0,1";
		
		#execute query
		$dbSummonOne = new SQL($qr);
		$result = $dbSummonOne->fetchOneRow();
		return $result;
	
	}
	function countAll(){
		#settle query
		$qr= "SELECT * FROM ".$this->tbl_name." ORDER BY accno ASC";
		
		#get the number of rows
		$dbCountRow = new SQL($qr);
		$result = $dbCountRow->countRow();
		return $result;
	}

	//function to delete product
	function byebye($cid){
		$qr ="DELETE FROM ".$this->tbl_name." WHERE cid=".$cid;
		
		#execute query
		$dbBye = new SQL($qr);

		return $dbBye->getUpdate();

	}





}
?>

	<html> <!--
	//this is a backup of old code


	//function to create product
	function create(){
		#settle query
		$qr = "INSERT INTO ".$this->tbl_name."
			   SET 
			   accno=:accno, co_name=:co_name, co_no=:co_no, co_code=:co_code, address1=:adr1, address2=:adr2, address3=:adr3, country=:country, telephone_sales=:salesphone, fax_sales=:salesfax, handphone_sales=:saleshandphone, email_sales=:salesemail, attn_sales=:salesatn, telephone_acc=:accphone, fax_acc=:accfax, handphone_acc=:acchandphone, email_acc=:accemail, attn_acc=:accatn, groups=:groups, aid_cus=:aid_cus, terms=:terms, currency=:currency, credit_limit=:creditlimit, company=:company, status=:status, date_created=:dtime, remarks=:remarks;";

		$stmt = $this->getConnection()->prepare($qr);

		#set time value
		$this->dtime = date('Y-m-d H:i:s');

		#echo "query : ".$qr."<br>";

		$stmt->bindParam(":accno",$this->accno);
		$stmt->bindParam(":co_name",$this->co_name);
		$stmt->bindParam(":co_no",$this->co_no);
		$stmt->bindParam(":co_code",$this->co_code);
		$stmt->bindParam(":adr1",$this->adr1);
		$stmt->bindParam(":adr2",$this->adr2);
		$stmt->bindParam(":adr3",$this->adr3);
		$stmt->bindParam(":country",$this->country);
		$stmt->bindParam(":salesphone",$this->salesphone);
		$stmt->bindParam(":salesfax",$this->salesfax);
		$stmt->bindParam(":saleshandphone",$this->saleshp);
		$stmt->bindParam(":salesemail",$this->salesmail);
		$stmt->bindParam(":salesatn",$this->salesatn);
		$stmt->bindParam(":accphone",$this->accphone);
		$stmt->bindParam(":accfax",$this->accfax);
		$stmt->bindParam(":acchandphone",$this->acchp);
		$stmt->bindParam(":accemail",$this->accmail);
		$stmt->bindParam(":accatn",$this->accatn);
		$stmt->bindParam(":groups",$this->groups);
		$stmt->bindParam(":aid_cus",$this->aid_cus);
		$stmt->bindParam(":terms",$this->terms);
		$stmt->bindParam(":currency",$this->currency);
		$stmt->bindParam(":creditlimit",$this->creditlimit);
		$stmt->bindParam(":company",$this->company);
		$stmt->bindParam(":status",$this->status);
		$stmt->bindParam(":dtime",$this->dtime);
		$stmt->bindParam(":remarks",$this->remarks);
;
		#try {
		#	$stmt->execute();
		#} catch (Exception $e) {
		#	echo $e->getMessage();
		#}
		
		$dbInsert = new SQL($stmt);
		if ($dbInsert->getUpdate()){
			$info = "Insert Successful";
		}else{
			$info = "Insert Failed";
		}
		return $info;
	}

	//function to update product
	function update($cid){
		#settle query
		$qr = "UPDATE ".$this->tbl_name."
			   SET 
			   accno=:accno,
			   co_name=:co_name,
			   co_no=:co_no,
			   co_code=:co_code,
			   address1=:adr1,
			   address2=:adr2,
			   address3=:adr3,
			   country=:country,
			   telephone_sales=:salesphone,
			   fax_sales=:salesfax,
			   handphone_sales=:saleshandphone,
			   email_sales=:salesemail,
			   attn_sales=:salesatn,
			   telephone_acc=:accphone,
			   fax_acc=:accfax,
			   handphone_acc=:acchandphone,
			   email_acc=:accemail,
			   attn_acc=:accatn,
			   groups=:groups,
			   aid_cus=:aid_cus,
			   terms=:terms,
			   currency=:currency,
			   credit_limit=:creditlimit,
			   company=:company,
			   status=:status,
			   remarks=:remarks WHERE cid=".$cid;

		#prepares query
		$stmt = $this->getConnection()->prepare($qr); 

		#binds values
		$stmt->bindParam(":accno",$this->accno);
		$stmt->bindParam(":co_name",$this->co_name);
		$stmt->bindParam(":co_no",$this->co_no);
		$stmt->bindParam(":co_code",$this->co_code);
		$stmt->bindParam(":adr1",$this->adr1);
		$stmt->bindParam(":adr2",$this->adr2);
		$stmt->bindParam(":adr3",$this->adr3);
		$stmt->bindParam(":country",$this->country);
		$stmt->bindParam(":salesphone",$this->salesphone);
		$stmt->bindParam(":salesfax",$this->salesfax);
		$stmt->bindParam(":saleshandphone",$this->saleshp);
		$stmt->bindParam(":salesemail",$this->salesmail);
		$stmt->bindParam(":salesatn",$this->salesatn);
		$stmt->bindParam(":accphone",$this->accphone);
		$stmt->bindParam(":accfax",$this->accfax);
		$stmt->bindParam(":acchandphone",$this->acchp);
		$stmt->bindParam(":accemail",$this->accmail);
		$stmt->bindParam(":accatn",$this->accatn);
		$stmt->bindParam(":groups",$this->groups);
		$stmt->bindParam(":aid_cus",$this->aid_cus);
		$stmt->bindParam(":terms",$this->terms);
		$stmt->bindParam(":currency",$this->currency);
		$stmt->bindParam(":creditlimit",$this->creditlimit);
		$stmt->bindParam(":company",$this->company);
		$stmt->bindParam(":status",$this->status);
		$stmt->bindParam(":remarks",$this->remarks);

		#execute query
		$dbUpdate = new SQL($stmt);

		if ($dbUpdate->getUpdate()){
			$info = "Update Successful";
		}else{
			$info = "Update Failed";
		}
		return $info;
	}

	//function to read product
	function summonAll(){
		#settle query
		$qr = "SELECT * FROM ".$this->tbl_name." ORDER BY accno ASC";
		#prepares query
		$stmt = $this->getConnection()->prepare($qr); 

		#execute query
		$dbSummon = new SQL($stmt);
		$result = $dbSummon->fetchAllRow();
		return $result;
	}
	function summonLimit($start, $count){
		#settle query
		$qr = "SELECT * FROM ".$this->tbl_name." ORDER BY accno ASC LIMIT ".$start.",".$count;
		#prepares query
		$stmt = $this->getConnection()->prepare($qr); 
		
		#execute query;
		$dbSummonLimit = new SQL($stmt);
		$result = $dbSummonLimit->fetchOneRow();
		return $result;

	}

	//function to read one data
	function summonOne($cid){
		#settle query
		$qr = "SELECT * FROM ".$this->tbl_name." WHERE cid=".$cid." LIMIT 0,1";
		#prepares query
		$stmt = $this->getConnection()->prepare($qr); 

		#execute query
		$dbSummonOne = new SQL($stmt);
		$result = $dbSummonOne->fetchOneRow();
		return $result;
	
	}
	function countAll(){
		#settle query
		$qr= "SELECT * FROM ".$this->tbl_name." ORDER BY accno ASC";
		#repares query
		$stmt = $this->getConnection()->prepare($qr);

		#get the number of rows
		$dbCountRow = new SQL($stmt);
		$result = $dbCountRow->countRow();
		return $result;
	}

	//function to delete product
	function byebye($cid){
		$qr ="DELETE FROM ".$this->tbl_name." WHERE cid=".$cid;
		#prepares query
		$stmt = $this->getConnection()->prepare($qr);

		#execute query
		$dbBye = new SQL($stmt);

		if ($dbBye->getUpdate()) {
			$info = "Delete Successful";
		}else{
			$info = "Delete Failed";
		}
		return $info;

	}
}

?> --> </html>