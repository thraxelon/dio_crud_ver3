<?php
include_once 'obj/customer.php';
$page_title = "Update Customer";

$id = isset($_GET['cid']) ? $_GET['cid'] : die('ERROR: You can\'t reach this page this way. <br> 
												 Please do it the  <a href="cus_list.php">right way</a>.');

include_once 'lib/pg_header.php';

$customer = new Customer();

#define customer variable for ease of access

$customer->accno = $_POST['accno']; 		#customer id
$customer->co_name = $_POST['co_name']; 	#customer/company name
$customer->co_no = $_POST['co_no'];		#customer Tax number
$customer->co_code = $_POST['co_code'];	#customer Initials
$customer->adr1 = $_POST['adr1']; 		#address1st  line
$customer->adr2 = $_POST['adr2'];		#address 2nd Line
$customer->adr3 = $_POST['adr3'];		#address 3rd line
$customer->country = $_POST['country'];	#company country
$customer->salesphone = $_POST['salesphone'];	#sales phone number
$customer->salesfax = $_POST['salesfax'];	#sales fax number
$customer->saleshp = $_POST['saleshp'];	#sales handphone number
$customer->salesmail = $_POST['salesmail'];	#sales email
$customer->salesatn = $_POST['salesattn'];	#sales name
$customer->accphone = $_POST['accphone'];	#accounting phone number
$customer->accfax = $_POST['accfax'];		#accounting fax number
$customer->acchp = $_POST['acchp'];		#accounting handphone number
$customer->accmail = $_POST['accmail'];	#accounting email
$customer->accatn = $_POST['accattn'];		#accounting name
$customer->groups = $_POST['group'];		#grouping code
$customer->aid_cus = $_POST['salesperson'];	#marketing code
$customer->terms = $_POST['terms'];		#payment terms
$customer->currency = $_POST['currency'];	#payment currency
$customer->creditlimit = $_POST['credit_limit'];#payment credit limits
$customer->company = $_POST['company'];	#company branch
$customer->status = $_POST['status'];		#customer status (active, hold, disabled)
$customer->remarks = $_POST['remarks'];	#remarksx

if ($customer->update($id)) {
	Echo "Customer has been successfully updated.";
}else{
	Echo "Failed to update Customer data.";
}
Echo "<br><a href='cus_list.php' class='btn btn-primary'>Click here to return</a>";

include_once "lib/pg_footer.php";
?>
